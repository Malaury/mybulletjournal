package com.example.mybulletjournal2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ProjectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
    }

    public void enterInProfile(View v){
        Intent intent = new Intent(this, AccountActivity.class);
        startActivity(intent);
    }

    public void enterInConfiguration(View v){
        Intent intent = new Intent(this, ConfigurationActivity.class);
        startActivity(intent);
    }

    public void enterInStatistic(View v){
        Intent intent = new Intent(this, StatistiqueActvity.class);
        startActivity(intent);
    }
}
