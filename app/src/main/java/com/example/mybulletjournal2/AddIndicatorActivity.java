package com.example.mybulletjournal2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AddIndicatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_indicator);
    }

    public void enterInConfiguration(View v){
        Intent intent = new Intent(this, ConfigurationActivity.class);
        startActivity(intent);
    }
}
