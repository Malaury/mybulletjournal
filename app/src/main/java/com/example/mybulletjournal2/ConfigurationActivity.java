package com.example.mybulletjournal2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConfigurationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
    }

    public void createIndicator(View v){
        Intent intent = new Intent(this, AddIndicatorActivity.class);
        startActivity(intent);
    }

    public void deleteIndicator(View v){
        DialogDeleteIndicator dialogDeleteIndicator = new DialogDeleteIndicator();
        dialogDeleteIndicator.show(getSupportFragmentManager(),"Warning");
    }

    public void updateIndicator(View v){
        Intent intent = new Intent(this, AddIndicatorActivity.class);
        startActivity(intent);
    }
}
